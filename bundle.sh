#!/bin/bash
BUTLER=~/bin/butler-darwin-amd64/butler

for i in {1..4}; do
    $BUTLER push ./CalicoDemo${i} crocmiam/fiction-interactive-fr-calico-demo-${i}:html5
done
