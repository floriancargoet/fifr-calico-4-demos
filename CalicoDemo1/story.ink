Bienvenue dans la première démo des fonctionnalités de Calico.

-> features

== features
- (opts)
Vous pouvez choisir une fonctionnalité de base pour voir comment elle fonctionne.

+ [\#class #prepend: <h4>Tags compatibles Inky</h4>] -> class ->
+ [\#image] -> image ->
+ [\#background] -> background ->
+ [\#clear] -> clear ->
+ [\#restart] -> restart ->
+ [\#delay #prepend: <h4>Tags Calico</h4>] -> delay ->
+ [\#linebyline] -> linebyline ->
-
#clear
- -> opts

== class
<h3>class</h3>
Ce tag est équivalent à celui de l'export web Inky et sert à attribuer une classe CSS à la ligne taggée.
- (opts)
+ [Demo]
    Cette ligne a la classe `blanc-sur-rouge` (rien ne bouge) #class: blanc-sur-rouge
    Cette ligne a la classe `rouge-sur-blanc` (tout fout le camp) #class: rouge-sur-blanc
+ [Retour]
    ->->
- -> opts

== image
<h3>image</h3>
Ce tag est équivalent à celui de l'export web Inky et permet d'afficher une image.
- (opts)
+ [Demo] #image: calico
+ [Retour]
    ->->
- -> opts

== background
<h3>background</h3>
Ce tag est équivalent à celui de l'export web Inky et permet d'afficher une image de fond.
- (opts)
+ [Demo] #background: calico
+ [Retour] #background
    ->->
- -> opts

== clear
<h3>clear</h3>
Ce tag est équivalent à celui de l'export web Inky et permet d'effacer l'écran.
- (opts)
+ [Demo] #clear
+ [Retour]
    ->->
- -> opts

== restart
<h3>restart</h3>
Ce tag est équivalent à celui de l'export web Inky et permet de redémarrer l'histoire.
- (opts)
+ [Demo] #restart
+ [Retour]
    ->->
- -> opts

== delay
<h3>delay</h3>
Ce tag inclus par défaut dans Calico insère une pause avant d'afficher la suite du texte.
- (opts)
+ [Demo]
    Merci d'avoir lancé la démonstration
    Chargement en cours #delay: 800
    Préparez vous à être impressionnés #delay: 800
    ... #delay: 800
    ⚠️ Échec du chargement ⚠️ #delay: 800
    -- #delay: 800
+ [Retour]
    ->->
- -> opts

== linebyline
<h3>linebyline</h3>
Ce tag inclus par défaut dans Calico permet d'arrêter l'histoire à chaque ligne. Cependant c'est à vous d'ajouter du code pour reprendre l'histoire après la pause.
- (opts)
+ [Demo] #linebyline
    linebyline activé, appuyez sur ESPACE ou cliquez pour passer à la ligne suivante
    Calico s'arrête désormais à chaque ligne
    On peut le désactiver à volonté
    Mais je vais continuer encore un peu
    Car ça m'amuse
    Voilà, je vous libère #linebyline
+ [Retour]
    ->->
- -> opts
