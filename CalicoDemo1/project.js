

// create our game
const story = new Story("story.ink");
window.story = story;

Patches.add(function () {
  // Bind click & space to continue when in lineByLine mode
  let needsResume = false;
  function tryResume(event) {
    if (needsResume) {
      needsResume = false;
      story.queue.render();
    }
  }
  Shortcuts.add(" ", tryResume);
  this.outerdiv.addEventListener("click", tryResume);
  this.outerdiv.addEventListener("render interrupted", () => {
    needsResume = true;
  });
});

// Insert content between choices
Parser.tag("prepend", function (line, tag, value) {
  const p = document.createElement("p");
  // Pretend it's a choice so it doesn't interfere with Calico
  // Undo styling in fake-choice
  p.classList.add("choice", "fake-choice");
  p.innerHTML = value;
  story.queue.push(p);
});
