+ [Ouvrir l'application Messagerie] #clear
-

Salut, t'es prêt pour vendredi ? #class: inconnu 
+ Je ne reconnais pas ce numéro, qui êtes vous ? [] #class: joueur #playonce: envoi
    Fais pas l'andouille, c'est @smwhr. Bon, il est prêt ton article ? #class: inconnu #delay: 1000
+ Vendredi ? Il se passe quoi vendredi ? [] #class: joueur #playonce: envoi
    Ton article pour fiction-interactive.fr ? #class: inconnu #delay: 1000
    Au fait, c'est @smwhr, je sais pas si t'avais mon numéro. #class: inconnu #delay: 500
+ De ouf que je suis prêt ! Mais t'es qui ? [] #class: joueur #playonce: envoi
    C'est @smwhr et on attend ton article. Tu m'as supprimé de tes contacts ou quoi ? #class: inconnu #delay: 1000
-
+ Ouais, ouais, mon article est prêt [] #class: joueur #playonce: envoi
    Enfin ! On peut publier alors ! #class: inconnu #delay: 1000
+ Juste une petite démo à ajouter [] #class: joueur #playonce: envoi
    Magne toi, t'as déjà loupé le créneau de vendredi dernier… #class: inconnu #delay: 1000
-
Fin de cette histoire épique
+ [Recommencer] #restart
