import "./patches/audioplayer.js";
import "./patches/preload.js";
import "./patches/minwordsperline.js";
import "./patches/stepback.js";
import choices from "./patches/shortcuts/choices.js";
// Ce patch n'est pas encore dispo sur la 2.0.3 mais devrait l'être dans la prochaine version.
// Il est facile d'aller le récupérer sur le GitHub du projet Calico.
import "./patches/fadeafterchoice.js"

// On veut précharger les sons.
options.preload_tags.audio.push("playonce");
// On n'affiche la barre de chargement que si ça prend plus de 500ms.
options.preload_timeout = 500;

// On supprime le fondu par défaut
options.audioplayer_fadein = 0;
options.audioplayer_fadeout = 0;

// Les anciens paragraphes sont semi-transparents pour mieux visualiser les nouvaux.
options.fadeafterchoice_fadelevel = 50;

// On connecte les boutons 1 à 3 aux 3 premiers choix Ink.
choices.add("1", 0, true);
choices.add("2", 1, true);
choices.add("3", 2, true);

// On crée la story
var story = new Story("story.ink");

// En bonus, un aperçu d'une fonctionnalité avancée dont je n'ai pas parlé !
// On peut facilement détecter n'importe quelle touche et déclencher une action.
// Ici, les flèches gauche et droite permettent de revenir en arrière ou en avant.
Shortcuts.add("ArrowLeft", () => story.stepBack());
Shortcuts.add("ArrowRight", () => story.stepForwards());
