EXTERNAL get(var)
EXTERNAL set(var, value)

// Disable autosave only if we're not resuming from autosave
#eval: this.options.autosave_enabled = false;
#eval: hideMenu()

Bienvenue dans la troisième démo des fonctionnalités de Calico.
Cette démo aborde les fonctionnalités fournies par les <i>patches</i> qui nécessitent du code supplémentaire pour fonctionner et être utile.

-> patches

== patches
- (opts)
Vous pouvez choisir un patch pour voir comment il fonctionne.

+ [autosave] -> autosave ->
+ [eval] -> eval ->
+ [shortcuts/choices] -> shortcuts_choices ->
+ [stepback] -> stepback ->
+ [storage, history, memorycard] -> storage ->

-
#clear
- -> opts

== autosave
<h3>autosave</h3>
Et oui, je place celui ci dans les deux catégories. Il fonctionne tout seul mais pour vraiment en tirer partie, il faut travailler un peu. Au minimum, ajouter dans le HTML un bouton <i>Recommencer</i> pour ne pas être coincé dans une sauvegarde sans possibilité de revenir à zéro. Vous voudrez probablement aussi changer la méthode de stockage. Par défaut, c'est du <i>session storage</i>, ce qui veut dire que la sauvegarde est conservé si on recharge la page mais est perdue dès qu'on ferme l'onglet. <b>autosave</b> utilise le patch <b>memorycard</b> pour la sauvegarde et c'est l'option <i>memorycard_format</i> qui permet de changer de format. Vous avez trois options : <i>cookies</i>, <i>session</i>, <i>local</i>.
#eval: this.options.autosave_enabled = true; // Activate it before the demo so it can save the "Demo" choice.
- (opts)
+ [Démo] #eval: showMenu("\#restart")
    L'autosave a été activé. Sauf qu'il n'y a plus de choix. Le seul moyen de sortir de là est le lien "Recommencer".
    -> END
+ [Retour]
    #eval: hideMenu()
    #eval: this.options.autosave_enabled = false; storage.remove.call(story, "save");
    ->->
- -> opts

= if_reload
Si vous rechargez la page, vous reviendrez à ce choix.
->->


== eval
<h3>eval</h3>
L'usage de ce patch n'est pas recommandé par la développeuse de Calico mais je pense qu'il peut être utile pour faire des choses simples rapidement.
En activant ce patch, on peut exécuter du JavaScript directement depuis le code Ink avec le tag \#eval.
Écrire son propre patch est la meilleure façon d'ajouter une fonctionnalité mais dans le rush d'une gamejam, on n'a pas toujours le temps de faire les choses bien.

- (opts)
+ [Démo]
    ++ [\#eval: alert("coucou")] #eval: alert("coucou")
+ [Retour]
    ->->
- -> opts


== shortcuts_choices
<h3>shortcuts/choices</h3>
Ce patch permet d'associer des touches du clavier aux choix Ink. Par exemple la touche 1 pour le choix 1, la 2 pour le choix 2… Ou A, B, C… Ou la barre d'espace quand qu'il n'y a qu'un seul choix possible.

- (opts)
+ [Démo]
++ [Appuyez sur 1 pour le choix 1] Choix 1
+++ [Appuyez sur Espace ou 1 pour ce choix unique] Choix unique
++ [Appuyez sur 2 pour le choix 2] Choix 2
+++ [Appuyez sur Espace ou 1 pour ce choix unique] Choix unique
+ [Retour]
    ->->
- -> opts


== stepback
<h3>stepback</h3>
<b>stepback</b> ajoute la possibilité de revenir en arrière (dans la pratique, il redémarre l'histoire et rejoue tous les choix qu'il a mémorisé, sauf le dernier). Sauf que cette fonctionnalité n'est pas directement accessible au joueur. C'est à vous d'ajouter un bouton ou autre mécanisme pour déclencher ce retour en arrière. Ce patch permet aussi de faire un retour en avant.

// start by disabling undo so you can't escape the undo demo
#eval: hideMenu()
- (opts)
+ [Démo] #eval: showMenu("\#undo")
Cliquez sur le lien "Annuler" en haut à gauche pour revenir en arrière.
++ A
+++ B
++++ C
+ [Retour] #eval: hideMenu()
    ->->
- -> opts

== storage
<h3>storage, history, memorycard</h3>
Je regroupe ces trois là ensemble car ils sont surtout utiles si vous voulez créer vos propres plugins.
<b>memorycard</b> et <b>storage</b> sont utilisés par <b>autosave</b> mais vous pourriez vous en servir pour ajouter des boutons manuels Sauvegarder/Charger.
<b>history</b> est utilisé par <b>autosave</b> et <b>stepback</b>. Vous pourriez l'utiliser pour afficher l'ensemble des choix précédents et permettre de revenir à n'importe quel choix précédent.

- (opts)
+ [Démo sauvegarde] #eval: showMenu("\#save"); showMenu("\#load")
++ A
+++ B
++++ C
--
++ [Fin de la démo sauvegarde] #eval: hideMenu()
+ [Démo historique] #enable-history
++ Tourner à gauche
++ Tourner à droite
--
++ Monter
++ Descendre
--
++ Avancer
++ Reculer
--
++ [Fin de la démo historique] #disable-history
+ [Retour]
    ->->
- -> opts
