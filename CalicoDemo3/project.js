import "./patches/autosave.js";
import "./patches/eval.js";
import "./patches/stepback.js";
import choices from "./patches/shortcuts/choices.js";
import memorycard from "./patches/memorycard.js";
import storage from "./patches/storage.js";
import history from "./patches/history.js";

// bind the number keys to choices
for (let i = 0; i < 9; i++) {
  choices.add(String(i + 1), i, true);
}
choices.add(" ", 0, true, true);

options.eval_enabled = true;

// create our game
const story = new Story("story.ink");
// Expose some stuff as globals
Object.assign(window, {
  story,
  storage,
  historyPatch: history,
  hideMenu,
  showMenu,
});

document.getElementById("undo").addEventListener("click", (ev) => {
  ev.preventDefault();
  story.stepBack();
});

document.getElementById("restart").addEventListener("click", (ev) => {
  ev.preventDefault();
  // Restart is broken if the story is waiting.
  story.state = Story.states.idle;
  story.restart();
});

document.getElementById("save").addEventListener("click", (ev) => {
  ev.preventDefault();
  memorycard.save(story, "fifr-demo-3b"); // different save than autosave
});

document.getElementById("load").addEventListener("click", (ev) => {
  ev.preventDefault();
  story.clear();
  memorycard.load(story, "fifr-demo-3b"); // different save than autosave
  story.outerdiv.addEventListener(
    "render start",
    // No pause for first line
    (ev) => {
      ev.detail.queue.contents[0].delay = 0;
    },
    { once: true }
  );
});

function hideMenu(selector = "a") {
  document
    .querySelectorAll(`#menu > ${selector}`)
    .forEach((item) => (item.style.display = "none"));
}
function showMenu(selector = "a") {
  document
    .querySelectorAll(`#menu > ${selector}`)
    .forEach((item) => (item.style.display = "block"));
}

// History
let history_enabled = false;
let choice_history = [];
Patches.add(function () {
  this.outerdiv.addEventListener("passage end", ({ detail: { choice } }) => {
    if (history_enabled) {
      choice_history.push({
        text: choice.text,
        turn: this.ink.state.currentTurnIndex + 1,
      });
      refreshHistory();
    }
  });
});

Tags.add("enable-history", () => {
  history_enabled = true;
});
Tags.add("disable-history", () => {
  history_enabled = false;
  choice_history = [];
  refreshHistory();
});

const historyEl = document.getElementById("history");
function refreshHistory() {
  historyEl.innerHTML = choice_history
    .map((c) => `<a href="#" data-turn="${c.turn}">Annuler "${c.text}"</a>`)
    .join("");
}
historyEl.addEventListener("click", (ev) => {
  if (ev.target.dataset.turn) {
    const index = Array.from(historyEl.childNodes).indexOf(ev.target);
    if (index != -1) {
      choice_history = choice_history.slice(0, index);
      refreshHistory();
    }
    story.clear();
    history.load(story, Number(ev.target.dataset.turn));
  }
});
