import "./patches/audioplayer.js";
import "./patches/autosave.js";
import "./patches/dragtoscroll.js";
import "./patches/eval.js";
import "./patches/markdowntohtml.js";
import "./patches/minwordsperline.js";
import "./patches/parallaxframes.js";
import "./patches/preload.js";
import "./patches/scrollafterchoice.js";
import "./patches/shorthandclasstags.js";
import storage from "./patches/storage.js";

options.preload_tags.audio.push(
  // audioplayer
  "play",
  "playonce",
);
options.preload_tags.image.push("frame");
options.preload_timeout = 10;

options.eval_enabled = true;

options.shorthandclasstags_tags.push("pouet");

options.audioplayer_allowmultipletracks = true;
options.audioplayer_fadein = 1;
options.audioplayer_fadeout = 1;

options.scrollafterchoice_scrollTargetPadding = 0;

// create our game
const story = new Story("story.ink");
// Expose some stuff as globals
Object.assign(window, {
  story,
  // Expose storage so it's accessible from #eval
  storage
});

Patches.add(function () {
  // Run the preload demo when clicking on the associated button
  this.outerdiv.addEventListener("click", (event) => {
    if (event.target.id === "preload-btn") {
      runPreloadDemo();
    }
  });
});

let timeout1, timeout2, timeout3, progressContainer;
function clearPreloadDemo() {
  clearTimeout(timeout1);
  clearTimeout(timeout2);
  clearTimeout(timeout3);
  progressContainer && progressContainer.remove();
}

function runPreloadDemo() {
  clearPreloadDemo();
  progressContainer = document.createElement("div");
  progressContainer.classList.add("progresscontainer");
  const bar = document.createElement("div");
  bar.classList.add("progressbar");
  bar.style.width = "0%";
  bar.style.transition =
    "width " +
    story.options.preload_widthtransitionspeed +
    "ms linear, opacity " +
    story.options.preload_opacitytransitionspeed +
    "ms ease-out " +
    story.options.preload_opacitytransitiondelay +
    "ms";

  progressContainer.appendChild(bar);
  story.outerdiv.appendChild(progressContainer);

  timeout1 = setTimeout(() => (bar.style.width = "20%"), 0);
  timeout2 = setTimeout(() => (bar.style.width = "60%"), 500);
  timeout3 = setTimeout(async () => {
    await transition(
      bar,
      "width",
      "100%",
      story.options.preload_widthtransitionspeed,
      "0ms"
    );
    await transition(
      bar,
      "opacity",
      "0",
      story.options.preload_opacitytransitionspeed,
      story.options.preload_opacitytransitiondelay
    );
    clearPreloadDemo();
  }, 1500);
}
