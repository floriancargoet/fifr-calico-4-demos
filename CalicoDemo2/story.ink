EXTERNAL get(var)
EXTERNAL set(var, value)

VAR audioplayer_playing = false
VAR audioplayer_paused = false

// Disable autosave only if we're not resuming from autosave
#eval: this.options.autosave_enabled = false;

Bienvenue dans la deuxième démo des fonctionnalités de Calico.
Cette démo aborde les fonctionnalités fournies par les _patches_ qui ne nécessitent pas de code supplémentaire.

-> patches

== patches
- (opts)
Vous pouvez choisir un patch pour voir comment il fonctionne.

+ [autosave] -> autosave ->
+ [dragtoscroll] -> dragtoscroll ->
+ [markdowntohtml] -> markdowntohtml ->
+ [minwordsperline] -> minwordsperline ->
+ [audioplayer] -> audioplayer ->
+ [parallaxframes] -> parallaxframes ->
+ [preload] -> preload ->
+ [scrollafterchoice] -> scrollafterchoice ->
+ [shorthandclasstags] -> shorthandclasstags ->
+ [storage] -> storage ->

-
#clear
- -> opts

== autosave
<h3>autosave</h3>
Ce patch sauvegarde automatiquement la partie après chaque choix.
Attention : par défaut il n'y a pas de bouton restart, donc pour pleinement profiter de ce patch, il faudra probablement écrire un peu de code. Par défaut la sauvegarde ne dure que le temps d'une session (elle est conservée lors d'un rechargement de la page mais est perdue dès qu'on ferme l'onglet).
#eval: this.options.autosave_enabled = true; // Activate it before the demo so it can save the "Demo" choice.
- (opts)
+ [Demo]
    L'autosave a été activé. Si vous rechargez la page, vous reviendrez où vous étiez rendu.
    Il sera désactivé lorsque vous cliquerez sur Retour.
    ++ Choix A
        -> if_reload ->
        +++ Choix A1
            -> if_reload ->
        +++ Choix A2
            -> if_reload ->
    ++ Choix B
        -> if_reload ->
        +++ Choix B1
            -> if_reload ->
        +++ Choix B2
            -> if_reload ->
+ [Retour]
    #eval: this.options.autosave_enabled = false; storage.remove.call(story, "save");
    ->->
- -> opts

= if_reload
Si vous rechargez la page, vous reviendrez à ce choix.
->->

== dragtoscroll
<h3>dragtoscroll</h3>
Ce patch ajoute la possibilité de scroller en faisant un cliquer-glisser avec la souris.
Pour avoir de quoi scroller si vous voulez tester, je vous ai mis un peu de lorem ipsum.

- (opts)
+ [Lorem ipsum]
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus rhoncus malesuada turpis, sed semper dolor tempus ultricies. Sed condimentum enim eu erat pulvinar, sed sagittis neque lobortis. Pellentesque accumsan lacinia odio eget aliquam. Cras a ligula eget arcu varius auctor in a mi. Aenean varius id felis imperdiet fringilla. Mauris pharetra orci non magna dictum faucibus. Praesent ac ornare arcu. Aliquam id quam et elit imperdiet lacinia et nec leo. Suspendisse maximus suscipit sapien, nec lacinia libero semper in. Pellentesque luctus cursus ex, in pretium eros posuere eu. Nulla auctor mattis dolor eu euismod.

Praesent velit erat, luctus accumsan urna id, convallis dapibus tellus. Fusce mattis dolor nec dapibus pretium. Maecenas scelerisque metus vitae rutrum ultricies. Aenean molestie vehicula ligula, vitae sodales magna condimentum molestie. Fusce quis orci ut lorem pretium dictum id quis mi. Donec laoreet ornare nisl. Integer ipsum massa, viverra sit amet mauris in, ultricies tempus risus. Morbi consectetur urna nec enim facilisis, id iaculis purus blandit. Morbi pretium justo sed arcu consectetur suscipit. Ut eu sollicitudin libero. Suspendisse potenti. Donec congue euismod suscipit. Sed dapibus pharetra molestie. Praesent gravida mi id elementum fermentum. Maecenas et leo condimentum, aliquam mauris nec, gravida augue.

Maecenas commodo feugiat lectus id auctor. Ut gravida tristique felis eget tincidunt. Nunc pellentesque mi ut tortor lacinia, non tincidunt lacus elementum. Curabitur a eleifend elit. Morbi lectus risus, aliquet ut risus sit amet, semper euismod ipsum. Proin accumsan lectus eu sem interdum aliquet. Proin pulvinar euismod orci, vitae molestie lorem malesuada quis. Cras suscipit, diam eu dictum ultricies, purus erat finibus lorem, id suscipit turpis eros ac tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Fusce malesuada at ante in aliquet. Proin massa lorem, varius nec egestas et, porttitor sed mi. Integer urna justo, sodales non enim non, elementum egestas purus. Nam dolor nisi, mattis eget tempus non, ullamcorper non nibh.

Nam et nulla eu nibh convallis aliquam in sit amet orci. Mauris et ante tempor, convallis ipsum id, hendrerit arcu. Fusce in nisi pellentesque, semper mi in, posuere justo. Donec sed placerat dui, a commodo libero. Aliquam a erat justo. Vestibulum malesuada ipsum at turpis dapibus malesuada. Donec at ex id elit convallis finibus. Quisque auctor magna ut finibus aliquet. Phasellus laoreet elementum rhoncus. Mauris nec bibendum arcu. Nam mauris eros, pretium sit amet iaculis at, scelerisque at ante. Etiam molestie enim elementum laoreet viverra. Mauris vestibulum, magna congue rhoncus luctus, risus nisi euismod urna, sed sagittis massa dui vel libero. Nunc egestas magna metus, vel consectetur lorem commodo eget. Nam eleifend urna vitae fermentum auctor.

Aliquam mollis venenatis turpis, id fringilla ipsum cursus quis. Aliquam sed sem at arcu mollis consectetur sit amet eget tortor. Curabitur efficitur, odio vel auctor hendrerit, ante mi porta purus, in auctor metus turpis nec orci. Aenean nibh arcu, interdum et vestibulum ut, viverra eu sem. Nunc sit amet tortor nisi. Nam sapien nisi, ultrices ut ultricies in, finibus non augue. Curabitur malesuada orci sed rutrum lacinia. Sed in felis feugiat, tristique nisl in, pulvinar quam. Praesent egestas lectus purus, sed mattis leo feugiat eget. Vivamus sapien sapien, lobortis non massa sit amet, ullamcorper sodales tellus. Morbi id ultricies massa.

+ [Retour]
    ->->
- -> opts

== markdowntohtml
<h3>markdowntohtml</h3>
Ce patch permet d'utiliser le format Markdown dans ink. Bien sûr, il faudra échapper avec un "\\" les caractères qui entrent en conflit avec la syntaxe ink.
\#\\\#Titre
\#\#\\\#\\\#Sous-titre
\#\#\#\\\#\\\#\\\#Section
__\\_\\_Gras\\_\\___
_\\_Italique\\__
___\\_\\_\\_Gras & italique\\_\\_\\____
&\#96;`Chasse fixe`&\#96;
Règle horizontale : \\\---
\---
Lien : \\\[Fiction-interactive.fr](http:\/\/fiction-interactive.fr)
[Fiction-interactive.fr](http:\/\/fiction-interactive.fr)
- (opts)
+ [Retour]
    ->->
- -> opts

== minwordsperline
<h3>minwordsperline</h3>
Un patch bien spécique : il évite les mots orphelins sur une ligne en fin de paragraphe. Par défaut il s'arrange pour qu'il y ait au minimum deux mots sur une ligne.
Pour le voir à l'œuvre, redimensionnez la largeur de votre navigateur.
- (opts)
+ [Retour]
    ->->
- -> opts

== audioplayer
<h3>audioplayer</h3>
Une alternative à audiohandler pour gérer le son, ce patch ajoute quelques tags : \\\#play \\\#playonce \\\#pause \\\#resume \\\#stop
Plusieurs points auxquels faire attention :
\- \\\#pause ne met pas vraiment pause, il met le volume à 0
\- Certains navigateurs obligent à avoir une interaction avec la page avant d'autoriser à jouer des sons
\- Sur iOS le son ne fonctionne pas si vous êtes en mode silencieux (sauf avec des écouteurs)
Quelques avantages :
\- On peut jouer plusieurs sons en même temps (option à activer)
\- On peut démarrer et arrêter la musique avec un fondu
- (opts)
+ {not audioplayer_playing} [Lancer une musique de fond] #play: stomping-rock-four-shots-111444
    {Music by <a href="https:\/\/pixabay.com\/users\/alexgrohl-25289918\/">AlexGrohl<\/a>|}
    ~ audioplayer_playing = true
    ~ audioplayer_paused = false
+ {audioplayer_playing} [Arrêter la musique de fond] #stop: stomping-rock-four-shots-111444
    ~ audioplayer_playing = false
    ~ audioplayer_paused = false
+ {audioplayer_playing and not audioplayer_paused} [Pause] #pause: stomping-rock-four-shots-111444
    ~ audioplayer_paused = true
+ {audioplayer_playing and audioplayer_paused} [Reprendre] #resume: stomping-rock-four-shots-111444
    ~ audioplayer_paused = false
+ [Jouer un son une fois] #playonce: sheep-122256
    {Sound Effect by <a href="https:\/\/pixabay.com\/users\/studioalivioglobal-28281460\/">StudioAlivioGlobal<\/a>| }
+ [Retour]
    ->->
- -> opts

== parallaxframes
<h3>parallaxframes</h3>
Ce patch permet de créer un effet de parallaxe avec plusieurs images.
- (opts)
+ [Demo]
#frame: parallax0:0, parallax1:0.5, parallax2:1, parallax3:3
+ [Retour]
    ->->
- -> opts

== preload
<h3>preload</h3>
Si vous utilisez des sons ou des images, ce patch est très utile : il les précharge avant de lancer l'histoire.
Il est activé pour cette démo, au chargement vous avez du apercevoir une barre de progression tout en haut de l'écran.
Si vous l'avez manqué, ça ressemble à ça : <button id="preload-btn">Démo</button>
- (opts)
+ [Retour]
    ->->
- -> opts

== scrollafterchoice
<h3>scrollafterchoice</h3>
Par défaut, quand on fait un choix, le texte apparait en dessous mais ne défile pas. Pour avoir un défilement automatique jusqu'au début du nouveau texte, il suffit d'inclure ce patch.
- (opts)
+ [Demo]
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus rhoncus malesuada turpis, sed semper dolor tempus ultricies. Sed condimentum enim eu erat pulvinar, sed sagittis neque lobortis. Pellentesque accumsan lacinia odio eget aliquam. Cras a ligula eget arcu varius auctor in a mi. Aenean varius id felis imperdiet fringilla. Mauris pharetra orci non magna dictum faucibus. Praesent ac ornare arcu. Aliquam id quam et elit imperdiet lacinia et nec leo. Suspendisse maximus suscipit sapien, nec lacinia libero semper in. Pellentesque luctus cursus ex, in pretium eros posuere eu. Nulla auctor mattis dolor eu euismod.

Praesent velit erat, luctus accumsan urna id, convallis dapibus tellus. Fusce mattis dolor nec dapibus pretium. Maecenas scelerisque metus vitae rutrum ultricies. Aenean molestie vehicula ligula, vitae sodales magna condimentum molestie. Fusce quis orci ut lorem pretium dictum id quis mi. Donec laoreet ornare nisl. Integer ipsum massa, viverra sit amet mauris in, ultricies tempus risus. Morbi consectetur urna nec enim facilisis, id iaculis purus blandit. Morbi pretium justo sed arcu consectetur suscipit. Ut eu sollicitudin libero. Suspendisse potenti. Donec congue euismod suscipit. Sed dapibus pharetra molestie. Praesent gravida mi id elementum fermentum. Maecenas et leo condimentum, aliquam mauris nec, gravida augue.
 ++ [Scroller après le choix] #eval: this.options.scrollafterchoice_enabled = true;
 ++ [Ne pas scroller] #eval: this.options.scrollafterchoice_enabled = false;
 --
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus rhoncus malesuada turpis, sed semper dolor tempus ultricies. Sed condimentum enim eu erat pulvinar, sed sagittis neque lobortis. Pellentesque accumsan lacinia odio eget aliquam. Cras a ligula eget arcu varius auctor in a mi. Aenean varius id felis imperdiet fringilla. Mauris pharetra orci non magna dictum faucibus. Praesent ac ornare arcu. Aliquam id quam et elit imperdiet lacinia et nec leo. Suspendisse maximus suscipit sapien, nec lacinia libero semper in. Pellentesque luctus cursus ex, in pretium eros posuere eu. Nulla auctor mattis dolor eu euismod.

Praesent velit erat, luctus accumsan urna id, convallis dapibus tellus. Fusce mattis dolor nec dapibus pretium. Maecenas scelerisque metus vitae rutrum ultricies. Aenean molestie vehicula ligula, vitae sodales magna condimentum molestie. Fusce quis orci ut lorem pretium dictum id quis mi. Donec laoreet ornare nisl. Integer ipsum massa, viverra sit amet mauris in, ultricies tempus risus. Morbi consectetur urna nec enim facilisis, id iaculis purus blandit. Morbi pretium justo sed arcu consectetur suscipit. Ut eu sollicitudin libero. Suspendisse potenti. Donec congue euismod suscipit. Sed dapibus pharetra molestie. Praesent gravida mi id elementum fermentum. Maecenas et leo condimentum, aliquam mauris nec, gravida augue.

+ [Retour] #eval: this.options.scrollafterchoice_enabled = false;
    ->->
- -> opts

== shorthandclasstags
<h3>shorthandclasstags</h3>
Un petit patch de confort, il permet d'écrire \\\#pouet au lieu de \\\#class: pouet.
- (opts)
+ [Demo]
    Cette ligne a la classe `pouet` #pouet
+ [Retour]
    ->->
- -> opts

== storage
<h3>storage</h3>
Celui ci sert surtout pour ceux qui veulent coder des fonctionnalités de sauvegarde mais il expose également côté Ink deux fonctions externes utiles : `get(nomDeVariable)` et `set(nomDeVariable, valeur)`. Ça pourrait servir à un jeu de type *Die & Retry* pour mémoriser le nombre d'essais par exemple.

- (opts)
+ [Demo]
~ temp count = 1 + get("storage-demo-count") // convert to number if it returns false
~ set("storage-demo-count", count)
Vous avez testé cette démo {count} fois.
+ [Retour]
    ->->
- -> opts
